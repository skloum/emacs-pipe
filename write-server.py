#! /usr/bin/env python3

import sys

while True:
    requested_size = int(sys.stdin.readline())
    write_size = sys.stdout.write("A" * requested_size)
    sys.stdout.flush()
