(defvar show-details nil)

;; Values to use for `read-process-output-max'
(defvar read-process-output-max-list
  `(
    4096               ;; default value
    ,(* 32 1024)       ;; 32k
    ,(* 64 1024)       ;; 64k
    ,(+ 1 (* 64 1024)) ;; 64k + 1
    ,(* 200 1024)      ;; 200k
    ,(* 1024 1024)     ;; 1M
    ,(* 2 1024 1024)   ;; 2M
    ))

(defun filter-set-max-chunk-size (proc string)
  (when show-details
    (message "... chunk size: %s" (length string)))
  (setq max-chunk-size (max (length string) max-chunk-size)))

(defun calculate-real-pipe-size (requested-pipe-size)
  (let* ((read-process-output-max requested-pipe-size)
         (max-chunk-size 0)
         (proc (make-process
                :name "calculate-pipe-size"
                :buffer nil
                :command '("write-server.py")
                :connection-type 'pipe
                :filter 'filter-set-max-chunk-size)))
    (when show-details
      (message "== Calculating real pipe size with read-process-output-max = %s ==" read-process-output-max))
    (process-send-string "calculate-pipe-size" (concat (number-to-string requested-pipe-size) "\n"))
    (accept-process-output proc)
    (delete-process proc)
    max-chunk-size))

(dolist (pipe-size read-process-output-max-list)
  (message "Real pipe size (with read-process-output-max = %s): %s" pipe-size (calculate-real-pipe-size pipe-size)))
